import React, { useEffect, useState } from "react";
import json from "../../db.json";

import { login } from "../../redux/actions";
import { connect } from "react-redux";

const auth = json.auth;

const Login = props => {
  const [user, setUser] = useState({
    emailId: "",
    password: ""
  });
  const [loginButtonState, changeLoginButtonState] = useState(true);

  const onChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    if (user.emailId !== "" && user.password !== "") {
      changeLoginButtonState(false);
    }
  }, [user.emailId, user.password]);

  const loginHandler = e => {
    e.preventDefault();
    if (
      user.emailId === auth[0].username &&
      user.password === auth[0].password
    ) {
      props.dispatch(login());
    }
  };

  return (
    <div>
      <h4>Login</h4>
      <br />
      <div style={styles.form} className="card form-card">
        <form>
          <div className="input-field">
            <input
              id="emailId"
              name="emailId"
              type="email"
              className="validate"
              onChange={onChange}
              value={user.emailId}
              required
            />
            <label htmlFor="emailId">Email</label>
          </div>
          <div className="input-field">
            <input
              id="password"
              name="password"
              type="password"
              className="validate"
              onChange={onChange}
              value={user.password}
              required
            />
            <label htmlFor="password">Password</label>
          </div>
          <div className="input-field">
            <button
              id="loginBtn"
              style={styles.button}
              type="button"
              className="btn waves-effect effect-light"
              onClick={loginHandler}
              disabled={loginButtonState}
            >
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

const styles = {
  form: {
    maxWidth: "500px",
    width: "90%",
    margin: "auto",
    padding: "1rem 2rem"
  },
  button: {
    width: "100%"
  }
};

export default connect()(Login);
