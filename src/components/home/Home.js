import React from "react";
import Login from "../login/Login";
import Users from "../users/Users";
import { connect } from "react-redux";

const Home = props => {
  const isLoggedIn = props.isLoggedIn;

  return <div className="container">{isLoggedIn ? <Users /> : <Login />}</div>;
};

const mapStateToProps = state => ({ isLoggedIn: state.isLoggedIn });

export default connect(mapStateToProps)(Home);
