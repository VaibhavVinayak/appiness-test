import React from "react";

const User = ({ name, age, gender, email, phoneNo }) => {
  return (
    <tr>
      <td>{name}</td>
      <td>{age}</td>
      <td>{gender}</td>
      <td>{email}</td>
      <td>{phoneNo}</td>
    </tr>
  );
};

export default User;
