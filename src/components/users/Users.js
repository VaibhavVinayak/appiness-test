import React from "react";
import json from "../../db.json";
import User from "./User.js";
import { logout } from "../../redux/actions.js";
import { connect } from "react-redux";

const Users = props => {
  const users = json.users;
  const logoutHandler = e => {
    e.preventDefault();
    props.dispatch(logout());
  };
  return (
    <div>
      <ul style={styles.table} className="collection with-header">
        <li className="collection-header">
          <h4>Users</h4>
        </li>
        <table className="highlight">
          <thead>
            <tr>
              <th width="20%">User</th>
              <th>Age</th>
              <th>Gender</th>
              <th>Email</th>
              <th>Phone</th>
            </tr>
          </thead>
          <tbody>
            {users &&
              users.map(user => (
                <User
                  key={user.id}
                  name={user.name}
                  age={user.age}
                  gender={user.gender}
                  email={user.email}
                  phoneNo={user.phoneNo}
                />
              ))}
          </tbody>
        </table>
      </ul>
      <div>
        <button onClick={logoutHandler} className="btn waves-effect">
          Logout
        </button>
      </div>
    </div>
  );
};

const styles = {
  table: {
    padding: "1rem"
  }
};

export default connect()(Users);
